"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var home_component_1 = require("~/app/home/home.component");
var equipe_component_1 = require("~/app/equipe/equipe.component");
var poemas_component_1 = require("~/app/poemas/poemas.component");
var noticias_component_1 = require("~/app/noticias/noticias.component");
var routes = [
    { path: "", component: home_component_1.HomeComponent },
    { path: "equipes", component: equipe_component_1.EquipeComponent },
    { path: "poemas", component: poemas_component_1.PoemasComponent },
    { path: "noticias", component: noticias_component_1.NoticiasComponent },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.NativeScriptRouterModule.forRoot(routes)],
            exports: [router_1.NativeScriptRouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLXJvdXRpbmcubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYXBwLXJvdXRpbmcubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQXlDO0FBQ3pDLHNEQUF1RTtBQUV2RSw0REFBMEQ7QUFDMUQsa0VBQWdFO0FBQ2hFLGtFQUErRDtBQUMvRCx3RUFBcUU7QUFHckUsSUFBTSxNQUFNLEdBQVc7SUFDbkIsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSw4QkFBYSxFQUFFO0lBQ3RDLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsa0NBQWUsRUFBRTtJQUMvQyxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsU0FBUyxFQUFFLGtDQUFlLEVBQUU7SUFDOUMsRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSxzQ0FBaUIsRUFBRTtDQUVyRCxDQUFDO0FBTUY7SUFBQTtJQUFnQyxDQUFDO0lBQXBCLGdCQUFnQjtRQUo1QixlQUFRLENBQUM7WUFDTixPQUFPLEVBQUUsQ0FBQyxpQ0FBd0IsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDbkQsT0FBTyxFQUFFLENBQUMsaUNBQXdCLENBQUM7U0FDdEMsQ0FBQztPQUNXLGdCQUFnQixDQUFJO0lBQUQsdUJBQUM7Q0FBQSxBQUFqQyxJQUFpQztBQUFwQiw0Q0FBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRSb3V0ZXJNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XG5pbXBvcnQgeyBSb3V0ZXMgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XG5pbXBvcnQgeyBIb21lQ29tcG9uZW50IH0gZnJvbSBcIn4vYXBwL2hvbWUvaG9tZS5jb21wb25lbnRcIjtcbmltcG9ydCB7IEVxdWlwZUNvbXBvbmVudCB9IGZyb20gXCJ+L2FwcC9lcXVpcGUvZXF1aXBlLmNvbXBvbmVudFwiO1xuaW1wb3J0IHtQb2VtYXNDb21wb25lbnQgfSBmcm9tIFwifi9hcHAvcG9lbWFzL3BvZW1hcy5jb21wb25lbnRcIjtcbmltcG9ydCB7Tm90aWNpYXNDb21wb25lbnQgfSBmcm9tIFwifi9hcHAvbm90aWNpYXMvbm90aWNpYXMuY29tcG9uZW50XCI7XG5cblxuY29uc3Qgcm91dGVzOiBSb3V0ZXMgPSBbXG4gICAgeyBwYXRoOiBcIlwiLCBjb21wb25lbnQ6IEhvbWVDb21wb25lbnQgfSxcbiAgICB7IHBhdGg6IFwiZXF1aXBlc1wiLCBjb21wb25lbnQ6IEVxdWlwZUNvbXBvbmVudCB9LFxuICAgIHsgcGF0aDogXCJwb2VtYXNcIiwgY29tcG9uZW50OiBQb2VtYXNDb21wb25lbnQgfSxcbiAgICB7IHBhdGg6IFwibm90aWNpYXNcIiwgY29tcG9uZW50OiBOb3RpY2lhc0NvbXBvbmVudCB9LFxuXG5dO1xuXG5ATmdNb2R1bGUoe1xuICAgIGltcG9ydHM6IFtOYXRpdmVTY3JpcHRSb3V0ZXJNb2R1bGUuZm9yUm9vdChyb3V0ZXMpXSxcbiAgICBleHBvcnRzOiBbTmF0aXZlU2NyaXB0Um91dGVyTW9kdWxlXVxufSlcbmV4cG9ydCBjbGFzcyBBcHBSb3V0aW5nTW9kdWxlIHsgfSJdfQ==