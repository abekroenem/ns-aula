import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { Routes } from "@angular/router";
import { HomeComponent } from "~/app/home/home.component";
import { EquipeComponent } from "~/app/equipe/equipe.component";
import {PoemasComponent } from "~/app/poemas/poemas.component";
import {NoticiasComponent } from "~/app/noticias/noticias.component";


const routes: Routes = [
    { path: "", component: HomeComponent },
    { path: "equipes", component: EquipeComponent },
    { path: "poemas", component: PoemasComponent },
    { path: "noticias", component: NoticiasComponent },

];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }