import { Component, OnInit } from '@angular/core';
import { ItemEventData } from "tns-core-modules/ui/list-view"

@Component({
  selector: 'ns-poemas',
  templateUrl: './poemas.component.html',
  styleUrls: ['./poemas.component.css'],
  moduleId: module.id,
})
export class PoemasComponent implements OnInit {

  countries: { name: string, altor: string }[] = [
    { name: "Livros são papéis pintados com tinta.Estudar é uma coisa em que está indistinta  A distinção entre nada e coisa nenhuma.", altor: " -Fernando Pessoa" },
    { name: "Às vezes, a única coisa verdadeira num jornal é a data.", altor: " -Luiz Fernando Verissimo" },
    { name: "Se fosse necessário estudar todas as leis, não teríamos tempo para as transgredir.", altor: " -Johann Goethe" },
    { name: "O que não dá prazer não dá proveito. Em resumo, senhor, estude apenas o que lhe agradar.", altor: " -William Shakespeare" },
];

onItemTap(args: ItemEventData): void {
    console.log('Item with index: ' + args.index + ' tapped');
}

constructor() {
}

ngOnInit(): void {
}
}