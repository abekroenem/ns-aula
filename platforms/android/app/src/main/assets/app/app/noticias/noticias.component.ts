import { ItemEventData } from "tns-core-modules/ui/list-view"
import { Component, OnInit } from "@angular/core";

@Component({
  selector: 'ns-noticias',
  templateUrl: './noticias.component.html',
  styleUrls: ['./noticias.component.css'],
  moduleId: module.id,
})
export class NoticiasComponent implements OnInit {

  countries: { name: string, imageSrc: string }[] = [
    { name: "Mulher é presa em Vilhena, RO, por maus tratos contra filhos de 3 anos e 4 meses", imageSrc: "https://s2.glbimg.com/pBZzOMzZI-7NHVQWUdy2lL-gcl8=/540x304/top/smart/https://i.s3.glbimg.com/v1/AUTH_59edd422c0c84a879bd37670ae4f538a/internal_photos/bs/2017/7/4/a4e9ZGRAAz3AGfvWbklg/dsc-0069.jpg" },
    { name: "'Alívio', diz família de piloto e copiloto resgatados em mata 4 dias após queda de avião em MT    ", imageSrc: "https://s2.glbimg.com/_OPjh4EA8hYI3oqgdNFphMx2-MM=/540x304/top/smart/https://i.s3.glbimg.com/v1/AUTH_59edd422c0c84a879bd37670ae4f538a/internal_photos/bs/2018/S/A/87rHc8S9ClnI2L7OPmBA/whatsapp-image-2018-12-04-at-22.55.39.jpeg" },
    { name: "Acadêmicos de RO se preparam para levar startups à competição mundial na Bélgica    ", imageSrc: "https://s2.glbimg.com/IWtfdEkmmT94g6iF8fzhQs03M7c=/540x304/top/smart/https://i.s3.glbimg.com/v1/AUTH_59edd422c0c84a879bd37670ae4f538a/internal_photos/bs/2018/F/T/82iYPbRB6Z4zAQGoSd0w/whatsapp-image-2018-11-27-at-16.14.44.jpeg" },
    { name: "Preço da cesta básica sobe 1,52% em Porto Velho; tomate é o item com maior alta", imageSrc: "https://s2.glbimg.com/PjYlYQfM28mZot0yYZotGELf5m8=/540x304/top/smart/https://i.s3.glbimg.com/v1/AUTH_59edd422c0c84a879bd37670ae4f538a/internal_photos/bs/2018/G/v/S4BDtuRt25ZQvsQ84Jeg/cesta-basica.jpg" },
];

onItemTap(args: ItemEventData): void {
    console.log('Item with index: ' + args.index + ' tapped');
}

constructor() {
}

ngOnInit(): void {
}
}