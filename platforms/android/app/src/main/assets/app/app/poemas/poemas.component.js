"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var PoemasComponent = /** @class */ (function () {
    function PoemasComponent() {
        this.countries = [
            { name: "Livros são papéis pintados com tinta.Estudar é uma coisa em que está indistinta  A distinção entre nada e coisa nenhuma.", altor: " -Fernando Pessoa" },
            { name: "Às vezes, a única coisa verdadeira num jornal é a data.", altor: " -Luiz Fernando Verissimo" },
            { name: "Se fosse necessário estudar todas as leis, não teríamos tempo para as transgredir.", altor: " -Johann Goethe" },
            { name: "O que não dá prazer não dá proveito. Em resumo, senhor, estude apenas o que lhe agradar.", altor: " -William Shakespeare" },
        ];
    }
    PoemasComponent.prototype.onItemTap = function (args) {
        console.log('Item with index: ' + args.index + ' tapped');
    };
    PoemasComponent.prototype.ngOnInit = function () {
    };
    PoemasComponent = __decorate([
        core_1.Component({
            selector: 'ns-poemas',
            templateUrl: './poemas.component.html',
            styleUrls: ['./poemas.component.css'],
            moduleId: module.id,
        }),
        __metadata("design:paramtypes", [])
    ], PoemasComponent);
    return PoemasComponent;
}());
exports.PoemasComponent = PoemasComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicG9lbWFzLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInBvZW1hcy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBa0Q7QUFTbEQ7SUFhQTtRQVhFLGNBQVMsR0FBc0M7WUFDN0MsRUFBRSxJQUFJLEVBQUUsMEhBQTBILEVBQUUsS0FBSyxFQUFFLG1CQUFtQixFQUFFO1lBQ2hLLEVBQUUsSUFBSSxFQUFFLHlEQUF5RCxFQUFFLEtBQUssRUFBRSwyQkFBMkIsRUFBRTtZQUN2RyxFQUFFLElBQUksRUFBRSxvRkFBb0YsRUFBRSxLQUFLLEVBQUUsaUJBQWlCLEVBQUU7WUFDeEgsRUFBRSxJQUFJLEVBQUUsMEZBQTBGLEVBQUUsS0FBSyxFQUFFLHVCQUF1QixFQUFFO1NBQ3ZJLENBQUM7SUFPRixDQUFDO0lBTEQsbUNBQVMsR0FBVCxVQUFVLElBQW1CO1FBQ3pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLEtBQUssR0FBRyxTQUFTLENBQUMsQ0FBQztJQUM5RCxDQUFDO0lBS0Qsa0NBQVEsR0FBUjtJQUNBLENBQUM7SUFqQlksZUFBZTtRQU4zQixnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLFdBQVc7WUFDckIsV0FBVyxFQUFFLHlCQUF5QjtZQUN0QyxTQUFTLEVBQUUsQ0FBQyx3QkFBd0IsQ0FBQztZQUNyQyxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7U0FDcEIsQ0FBQzs7T0FDVyxlQUFlLENBa0IzQjtJQUFELHNCQUFDO0NBQUEsQUFsQkQsSUFrQkM7QUFsQlksMENBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSXRlbUV2ZW50RGF0YSB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL2xpc3Qtdmlld1wiXG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25zLXBvZW1hcycsXG4gIHRlbXBsYXRlVXJsOiAnLi9wb2VtYXMuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9wb2VtYXMuY29tcG9uZW50LmNzcyddLFxuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxufSlcbmV4cG9ydCBjbGFzcyBQb2VtYXNDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGNvdW50cmllczogeyBuYW1lOiBzdHJpbmcsIGFsdG9yOiBzdHJpbmcgfVtdID0gW1xuICAgIHsgbmFtZTogXCJMaXZyb3Mgc8OjbyBwYXDDqWlzIHBpbnRhZG9zIGNvbSB0aW50YS5Fc3R1ZGFyIMOpIHVtYSBjb2lzYSBlbSBxdWUgZXN0w6EgaW5kaXN0aW50YSAgQSBkaXN0aW7Dp8OjbyBlbnRyZSBuYWRhIGUgY29pc2EgbmVuaHVtYS5cIiwgYWx0b3I6IFwiIC1GZXJuYW5kbyBQZXNzb2FcIiB9LFxuICAgIHsgbmFtZTogXCLDgHMgdmV6ZXMsIGEgw7puaWNhIGNvaXNhIHZlcmRhZGVpcmEgbnVtIGpvcm5hbCDDqSBhIGRhdGEuXCIsIGFsdG9yOiBcIiAtTHVpeiBGZXJuYW5kbyBWZXJpc3NpbW9cIiB9LFxuICAgIHsgbmFtZTogXCJTZSBmb3NzZSBuZWNlc3PDoXJpbyBlc3R1ZGFyIHRvZGFzIGFzIGxlaXMsIG7Do28gdGVyw61hbW9zIHRlbXBvIHBhcmEgYXMgdHJhbnNncmVkaXIuXCIsIGFsdG9yOiBcIiAtSm9oYW5uIEdvZXRoZVwiIH0sXG4gICAgeyBuYW1lOiBcIk8gcXVlIG7Do28gZMOhIHByYXplciBuw6NvIGTDoSBwcm92ZWl0by4gRW0gcmVzdW1vLCBzZW5ob3IsIGVzdHVkZSBhcGVuYXMgbyBxdWUgbGhlIGFncmFkYXIuXCIsIGFsdG9yOiBcIiAtV2lsbGlhbSBTaGFrZXNwZWFyZVwiIH0sXG5dO1xuXG5vbkl0ZW1UYXAoYXJnczogSXRlbUV2ZW50RGF0YSk6IHZvaWQge1xuICAgIGNvbnNvbGUubG9nKCdJdGVtIHdpdGggaW5kZXg6ICcgKyBhcmdzLmluZGV4ICsgJyB0YXBwZWQnKTtcbn1cblxuY29uc3RydWN0b3IoKSB7XG59XG5cbm5nT25Jbml0KCk6IHZvaWQge1xufVxufSJdfQ==